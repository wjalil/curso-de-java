package com.despegar.curso.visibilidad;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class UsuarioTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void loginValido() {
		UsuarioWeb usuario=new UsuarioWeb("wadi", "jalilmaluf");
		String inputPassword="jalilmaluf";
		assertTrue(usuario.login(inputPassword));
	}
	
	@Test
	public void loginInvalido() {
		UsuarioWeb usuario=new UsuarioWeb("wadi", "jalilmaluf");
		String inputPassword="pepe";
		assertFalse(usuario.login(inputPassword));
	}

}
