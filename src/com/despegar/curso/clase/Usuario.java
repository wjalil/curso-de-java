package com.despegar.curso.clase;

public class Usuario {

	public static int cantidadDeUsuarios=0;
	
	public static Usuario crearUsuario(String nombre,String apellido){
		return new Usuario(nombre,apellido);
	}
	
	protected String nombre;
	protected String apellido;
	
	public Usuario(String nombre,String apellido){
		this.nombre=nombre;
		this.apellido=apellido;
		cantidadDeUsuarios++;
	}
	
	@Override
	public String toString() {
		return apellido+", "+nombre;
	}
	
	
	
}
