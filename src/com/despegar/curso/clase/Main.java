package com.despegar.curso.clase;

public class Main {

	public static void main(String[] args){
		Triangulo miTriangull=Triangulo.crearTriangulo(40, 55);
		Usuario coronelPringlesOriginal=new Usuario("Coronel","Pringles");
		Usuario coronelPringles=new Usuario("Coronel","Pringles");
		
		if(coronelPringlesOriginal==coronelPringles)
			System.out.println("Son iguales..?");
		
		if(coronelPringlesOriginal.equals(coronelPringles))
			System.out.println("Son iguales");
		else
			System.out.println("No son iguales");
	}
	
}
