package com.despegar.curso.oop.encapsulamiento;

import java.util.ArrayList;
import java.util.List;

public class Pila {

	public List<Integer> list=new ArrayList<Integer>();
	
	public void apilar(Integer unNumero){
		list.add(unNumero);
	}
	
	public Integer desapilar(){	
		return (Integer) list.remove(list.size()-1);
	}
}
