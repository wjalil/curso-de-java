package com.despegar.curso.oop.encapsulamiento;

import java.util.ArrayList;
import java.util.LinkedList;

public class PilaConHerencia extends LinkedList{

	@Override
	public boolean add(Object e) {
		return super.add(e);
	}
	
	@Override
	public Integer getFirst() {
		return (Integer) super.getLast();
	}
}
