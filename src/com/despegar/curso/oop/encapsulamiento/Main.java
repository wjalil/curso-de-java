package com.despegar.curso.oop.encapsulamiento;

class Main {
public static void main(String[] args){
	Pila pila=new Pila();

	PilaConHerencia pilaConHerencia=new PilaConHerencia();
	pila.apilar(2);
	int dondeSeMeDeLaGana=2134;
	pilaConHerencia.add(dondeSeMeDeLaGana, 666);
}
}
