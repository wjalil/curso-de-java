package com.despegar.curso.oop.herencia;

public class Main {

	public static void main(String[] args){
		Usuario usuario=new Usuario("Jhon", "Doe");
		System.out.println(usuario.toString());
		UsuarioWeb usuarioWeb=new UsuarioWeb("Jhon", "Webdoe","jhon","jhon123");
		System.out.println(usuarioWeb.toString());
		usuario=usuarioWeb;
		System.out.println(usuario.toString());
		usuarioWeb=(UsuarioWeb) usuario;
		Main m=new Main();
		m.imprimirHola();
	}
	
	public  void imprimirHola(){
		//..
		/**
		 * 
		 */
		 
	}
	
}
