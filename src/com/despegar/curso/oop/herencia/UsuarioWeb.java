package com.despegar.curso.oop.herencia;

public class UsuarioWeb extends Usuario{

	private String usuario;
	private String contrasenia;
	
	

	public UsuarioWeb(String nombre,String apellido,String usuario,String password){
		super(nombre,apellido);
		this.usuario=usuario;
		this.contrasenia=password;
	}
	
	
	
	public String toString() {
		return super.toString() + " ("+usuario+","+contrasenia+")";
	}
	
}
