package com.despegar.curso.oop.herencia;

public class Usuario {

	private String nombre;
	protected String apellido;
	
	public Usuario(String nombre,String apellido){
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
	@Override
	public String toString() {
		return apellido+", "+nombre;
	}
	
	private void imprimirHola(){
		System.out.println("hola desde un usuaroi");
	}
}
