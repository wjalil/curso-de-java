package com.despegar.curso.oop.polimorfismo;

public class Main {
	
	public static void main(String[] args){
		Dibujo[] dibujos=new Dibujo[2];
		dibujos[0]=new Conejito();
		dibujos[1]=new Ratoncito();
		for (int i = 0; i < dibujos.length; i++) {
				dibujos[i].dibujar();
				System.out.println(".............................................................");
		}
	}
	
}
