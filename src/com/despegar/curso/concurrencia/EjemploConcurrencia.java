package com.despegar.curso.concurrencia;

public class EjemploConcurrencia {

	public static void main(String[] args){
		HiloQueSuma hiloQueCuenta=new HiloQueSuma();
		hiloQueCuenta.start();
		
		System.out.println("Ya termino");
		
		Runnable runnable=new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Hola desde un hilo");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Bu");
			}
		};
		Thread hilo=new Thread(runnable);
		hilo.start();
		
	}
	
}
