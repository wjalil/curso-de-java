package com.despegar.curso.exeptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Unchecked {

	
	public static void main(String[] args) {
//		try {
//			FileInputStream fis=new FileInputStream(new File(args[0]));
//		} catch (FileNotFoundException e) {
//			System.out.println("No se pudo abrir archivo");
//			e.printStackTrace();
//		}
		
		Object o=new Integer(2);
//		try {
			try {
				imprimirO(o);
			} catch (DespegarException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
			System.out.println("Changos"+ e.getMessage());
		}
		
		System.out.println("Finalizo ok");
	}

	private static void imprimirO(Object o) throws Exception {
			if(o==null)
				throw new DespegarException("O no puede ser nulo");
			throw new Exception("Otra excepción");
	}
	
}
