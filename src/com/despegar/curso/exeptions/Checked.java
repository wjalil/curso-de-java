package com.despegar.curso.exeptions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Checked {

	public static void main(String[] args){
		HttpURLConnection connection = null;
		try {
	        URL url = new URL("www.bergoogleo.com");
	        connection = (HttpURLConnection) url.openConnection();
	        connection.connect();
	        connection.getInputStream();
	                    // do something with the input stream here

	    } catch (MalformedURLException e1) {
	    	System.out.println("La url no es válida");
	        e1.printStackTrace();
	    } catch (IOException e1) {
	    	System.out.println("Hay un problema de conexión");
	        e1.printStackTrace();
	    } finally {
	    	//nos desconectamos
	        if(null != connection) { 
	        	connection.disconnect();
	        	}
	    }
		
		System.out.println("Seguimos con otras cosas");
	}
	
}
