package com.despegar.curso.parametros;

public class Main {

	public static void main(String[] args){
		int unNumeroFeliz=33;
		int otroNumero=11;
		intercambiar(unNumeroFeliz, otroNumero);
		System.out.println(unNumeroFeliz);
		System.out.println(otroNumero);
		
		Usuario manuelBelgrano=new Usuario("Manuel", "Belgrano");
		Usuario faustinoSarmiento=new Usuario("Faustino","Sarmiento");
		intercambiar(manuelBelgrano, faustinoSarmiento);
		
		intercambiar2(manuelBelgrano,faustinoSarmiento);		
		System.out.println(manuelBelgrano.toString());
		System.out.println(faustinoSarmiento);
		
		crearUsuario(manuelBelgrano);
		System.out.println(manuelBelgrano);
		
	}
	
	static void intercambiar(Usuario unUsuario,Usuario otroUsuario){
		Usuario aux=unUsuario;
		unUsuario=otroUsuario;
		otroUsuario=aux;
	}
	
	static void crearUsuario(Usuario unUsuario){
		unUsuario=new Usuario("Pepe", "Grillo");
	}
	
	static void intercambiar(int  unEntero,int otroEntero){
		int aux=unEntero;
		otroEntero=unEntero;
		unEntero=aux;
		
	}
	
	static void intercambiar2(Usuario unUsuario,Usuario otroUsuario){
		String auxNombre=unUsuario.nombre;
		String auxApe=unUsuario.apellido;
		unUsuario.nombre=otroUsuario.nombre;
		unUsuario.apellido=otroUsuario.apellido;
		otroUsuario.nombre=auxNombre;
		otroUsuario.apellido=auxApe;
				
	}
	
}
