package com.despegar.curso.parametros;

public class Usuario {

	protected String nombre;
	protected String apellido;
	
	public Usuario(String nombre,String apellido){
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
	public void setNombre(String nombre) {
		if(nombre==null)
			System.out.println("Nombre nulo!!!");
		this.nombre = nombre;
	}
	
	public String getNombre() {
		if(nombre==null){
			return "Juan Perez";
		}
		return nombre;
	}
	
	@Override
	public String toString() {
		if(nombre==null){
			return apellido;
		}
		return nombre+" "+apellido;
	}
}
