package com.despegar.curso.clasesinternas;

import com.despegar.curso.clasesinternas.Dialogo.OnClick;

public class Main {

	public static void main(String[] args){
		final String unDato="Pedro";
		Dialogo dialogo=new Dialogo("Hola", "Mundo");
		OnClick click=new Dialogo.OnClick() {
			
			/* (non-Javadoc)
			 * @see com.despegar.curso.clasesinternas.Dialogo.OnClick#onOkClicked()
			 */
			@Override
			public void onOkClicked() {
				
				System.out.println("Se hizo click en OK "+unDato);
			}
			
			@Override
			public void onCancelClicked() {
				System.out.println("Se hizo click en CANCEL "+unDato);
			}
		};

		dialogo.setListener(click);
		dialogo.show();
	}
	
}
