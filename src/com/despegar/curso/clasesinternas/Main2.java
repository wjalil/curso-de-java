package com.despegar.curso.clasesinternas;

import com.despegar.curso.clasesinternas.Dialogo.OnClickSayHello;

public class Main2 {
	public static void main(String[] args){
		final String unDato="Pedro";
		Dialogo dialogo=new Dialogo("Hola", "Mundo");
		Dialogo.OnClickSayHello listener =dialogo.new OnClickSayHello();
		dialogo.setListener(listener);
		dialogo.show();
	}
	
}