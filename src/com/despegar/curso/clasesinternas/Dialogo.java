package com.despegar.curso.clasesinternas;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Dialogo {

	public  interface OnClick{
		public void onOkClicked();
		public void onCancelClicked();
	}

	public  class OnClickSayHello implements OnClick{

		@Override
		public void onOkClicked() {
//			System.out.println("Se hizo clic en OK en el dialogo con mensaje: " + mensaje);
		}

		@Override
		public void onCancelClicked() {
//			System.out.println("Se hizo clic en OK en el dialogo con mensaje: " + mensaje);
		}
		
		
	}

	private String mensaje;
	private String titulo;
	private OnClick listener;
	
	public Dialogo(String titulo,String mensaje){
		this.titulo=titulo;
		this.mensaje=mensaje;
	}
	
	public void setListener(OnClick listener){
		this.listener=listener;
	}
	
	public void setMensaje(String mensaje){
		System.out.println(mensaje);
	}
	
	public void show(){
		
		
		int option =JOptionPane.showConfirmDialog(null, titulo,mensaje, JOptionPane.OK_CANCEL_OPTION);
		switch(option){
			case JOptionPane.CANCEL_OPTION:listener.onCancelClicked();break;
			case JOptionPane.OK_OPTION:listener.onOkClicked();break;
		}
	}
	
}
