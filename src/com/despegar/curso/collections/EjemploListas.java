package com.despegar.curso.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class EjemploListas {
	private static final int CANTIDAD_USUARIOS = 5;

	public static void main(String[] args){
		List<Usuario> usuarios=new LinkedList<Usuario>();
		aniadirUsuarios(usuarios);
		imprimirUsuariosConFuncionesDeListas(usuarios);
		System.out.println("***************************************************************************");
		System.out.println(usuarios.get(0));
		System.out.println("***************************************************************************");
		//Al final...
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		System.out.println("***************************************************************************");
		usuarios=new ArrayList<Usuario>();
		aniadirUsuarios(usuarios);
		aniadirUsuarios(usuarios);
		imprimirUsuarios(usuarios);
		System.out.println("***************************************************************************");
	}

	

	private static void imprimirUsuariosConFuncionesDeListas(List<Usuario> usuarios) {
		for (int i = 0; i < usuarios.size(); i++) {
			Usuario usuario=usuarios.get(i);
			System.out.println(usuario);
		}
	}


	private static void aniadirUsuarios(Collection<Usuario> usuarios) {
			for (int i = 0; i < CANTIDAD_USUARIOS; i++) {
				usuarios.add(new Usuario("Usuario",Integer.toString(i)));
			}
	}
	
	
	private static void imprimirUsuarios(Collection<Usuario> usuarios) {
		Iterator<Usuario>usuariosIterator=usuarios.iterator();
			while (usuariosIterator.hasNext()) {
				
				Usuario type = (Usuario) usuariosIterator.next();
				System.out.println(type.toString());
			}
	}
	
	
}
