package com.despegar.curso.collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class EjemploMaps {

	public static void main(String[] args){
		Map<String,Integer> edades=new HashMap<String, Integer>();
		edades.put("Wadi", 27);
		edades.put("Rodrigo", 22);
		edades.put("Maxi", 28);
		edades.put("Bernardo",Integer.MIN_VALUE);
		
		//luego puedo hacer
		System.out.println("La edad de Rodrigo es  "+edades.get("Rodrigo"));
		
	HashMap<String,Object> parametros=new HashMap<String,Object>();
	parametros.put("evolucion",new Usuario("Charles", "Darwin") );
	parametros.put("mutante",new Usuario("Charles", "Xavier"));
	parametros.put("java",new Usuario("James", "Gossling"));
	
	mostrarPersonajesMutantes(parametros);
	mostrarCreadorDeLaTeoriaDeLaEvolucion(parametros);
	
		
		
	}

	private static void mostrarCreadorDeLaTeoriaDeLaEvolucion(
			HashMap<String, Object> parametros) {
		Usuario usuario=(Usuario) parametros.get("evolucion");
		System.out.println(usuario);
	}

	private static void mostrarPersonajesMutantes(
			HashMap<String, Object> parametros) {
		Usuario usuario=(Usuario) parametros.get("mutante");
		System.out.println(usuario);
	}
	
}
