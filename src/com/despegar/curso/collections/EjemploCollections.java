package com.despegar.curso.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class EjemploCollections {

	private static final int CANTIDAD_USUARIOS = 5;

	public static void main(String[] args){
		Collection<Usuario> usuarios=new LinkedList<Usuario>();
		crearUsuarios(usuarios);
		imprimirUsuarios(usuarios);
	}

	

	private static void crearUsuarios(Collection<Usuario> usuarios) {
			for (int i = 0; i < CANTIDAD_USUARIOS; i++) {
				usuarios.add(new Usuario("Usuario",Integer.toString(i)));
			}
	}
	
	
	private static void imprimirUsuarios(Collection<Usuario> usuarios) {
		Iterator<Usuario>usuariosIterator=usuarios.iterator();
			while (usuariosIterator.hasNext()) {
				Usuario type = (Usuario) usuariosIterator.next();
				System.out.println(type.toString());
			}
	}
	
	
	
	
	
}
