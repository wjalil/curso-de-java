package com.despegar.curso.collections;

import java.util.HashSet;
import java.util.Iterator;

public class EjemploSets {

	public static void main(String[] args){
		HashSet<String> sets=new HashSet<String>();
		sets.add("Pedro");
		sets.add("Juan");
		sets.add("Carlos");
		sets.add("Carlos");
		sets.add("Carlos");
		sets.add("Javier");
		sets.add("Carlos");
		sets.add("Pedro");
		for (String string : sets) {
			System.out.println(string);
		}
		System.out.println("**********************************************************************************");
		Iterator<String>it=sets.iterator();
		while (it.hasNext()) {
			String string = (String) it.next();
			System.out.println(string);
		}		
	}
	
}
