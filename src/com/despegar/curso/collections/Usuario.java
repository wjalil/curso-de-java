package com.despegar.curso.collections;

public class Usuario {

	protected String nombre;
	protected String apellido;
	
	public Usuario(String nombre,String apellido){
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
	@Override
	public String toString() {
		return nombre+" "+apellido;
	}
}
