package com.despegar.curso.visibilidad;

public final class Cypher {

	private static final Integer KEY=25;
	
	String encriptar(String dataSinEncriptar){
		byte[] bytes=dataSinEncriptar.getBytes();
		for (byte b : bytes) {
			b=encriptarByte(b);
		}
		return new String(bytes);
	}
	
	String desencriptar(String dataEncriptada){
		byte[] bytes=dataEncriptada.getBytes();
		for (byte b : bytes) {
			b=desencriptarByte(b);
		}
		return new String(bytes);
	}
	
	protected final byte encriptarByte(byte byteSinEncriptar){
		if(byteSinEncriptar+KEY<Byte.MAX_VALUE){
			byteSinEncriptar=(byte) (byteSinEncriptar+KEY);
		}
		return byteSinEncriptar;
	}
	
	private byte desencriptarByte(byte byteEncriptado){
		if(byteEncriptado+KEY<Byte.MAX_VALUE){
			byteEncriptado=(byte) (byteEncriptado-1);
		}
		return byteEncriptado;
	}
	
	
}
