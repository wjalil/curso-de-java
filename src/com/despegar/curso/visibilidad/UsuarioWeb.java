package com.despegar.curso.visibilidad;

public class UsuarioWeb {

	protected String usuario;
	protected String contrasenia;
	
	protected Cypher cypher=new Cypher();	
	public UsuarioWeb(String usuario,String password){
		this.usuario=usuario;
		this.contrasenia=cypher.encriptar(password);
	}
	
	public boolean login(String inputPassword){
		Cypher cypher=new Cypher();
		String decryptedPassword=cypher.desencriptar(inputPassword);
		return (contrasenia.equals(decryptedPassword));
	}
	
	
	
	
	
}
