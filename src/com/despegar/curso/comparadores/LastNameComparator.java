package com.despegar.curso.comparadores;

import java.util.Comparator;

public class LastNameComparator implements Comparator<Usuario>{

	@Override
	public int compare(Usuario o1, Usuario o2) {
		return o1.apellido.compareToIgnoreCase(o2.apellido);
	}

}
