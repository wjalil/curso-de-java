package com.despegar.curso.comparadores;

public class Usuario implements Comparable<Usuario>{

	public static int cantidadDeUsuarios=0;
	
	public static Usuario crearUsuario(String nombre,String apellido){
		return new Usuario(nombre,apellido);
	}
	
	protected String nombre;
	protected String apellido;
	protected int id=0;
	Usuario(String nombre,String apellido){
		this.nombre=nombre;
		this.apellido=apellido;
		id=cantidadDeUsuarios++;
		Math.cos(0.5);
	}
	
	@Override
	public String toString() {
		return apellido+", "+nombre;
	}

	@Override
	public int compareTo(Usuario o) {
		if(id==o.id){
			return 0;
		}
		if(id<o.id)
			return -1;
		return 1;
	}
	
	
	
}
