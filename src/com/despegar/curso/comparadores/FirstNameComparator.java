package com.despegar.curso.comparadores;

import java.util.Comparator;

public class FirstNameComparator implements Comparator<Usuario>{

	@Override
	public int compare(Usuario o1, Usuario o2) {
		return o1.nombre.compareToIgnoreCase(o2.nombre);
	}

}
