package com.despegar.curso.comparadores;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {
	
	public static void main(String[] args){
		List<Usuario> usuarios=new LinkedList<Usuario>();
		
		usuarios.add(new Usuario("A", "p"));
		usuarios.add(new Usuario("B", "w"));
		usuarios.add(new Usuario("A", "l"));
		usuarios.add(new Usuario("Y", "z"));
		usuarios.add(new Usuario("l", "a"));
		usuarios.add(new Usuario("Q", "a"));
		usuarios.add(new Usuario("r", "r"));
		usuarios.add(new Usuario("A", "t"));
		System.out.println("****ORDENANDO POR ORDEN DE AGREGADO**********************");
		Collections.sort(usuarios);
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		System.out.println("****ORDENANDO POR NOMBRE**********************");
		Collections.sort(usuarios, new FirstNameComparator());
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		System.out.println("****ORDENANDO POR APELLIDO**********************");
		Collections.sort(usuarios, new LastNameComparator());
		for (Usuario usuario : usuarios) {
			System.out.println(usuario);
		}
		
	}

}
